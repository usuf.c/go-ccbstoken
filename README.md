# CCBS Token
**Create CCBS token client**
```
ccbsTokenClient := new(rediscache.CCBSTokenClient)
	ccbsTokenClient.Create(
		redis.Options{
			Addr:       "172.19.189.15:6379",
			PoolSize:   5,
			MaxRetries: 2,
			Password:   "testredis",
			DB:         1,
		},
		"http://172.19.198.26:15201/awsi_sec9/services/LoginService",
	)
```

**Request CCBS token**
```
token, err := ccbsTokenClient.Request("username", "password")
```