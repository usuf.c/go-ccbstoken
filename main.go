package main

import (
	"fmt"
	"log"

	"github.com/go-redis/redis"
	"gitlab.com/ft25/omx/go-ccbstoken/rediscache"
)

func main() {

	ccbsTokenClient := new(rediscache.CCBSTokenClient)
	ccbsTokenClient.Create(
		redis.Options{
			Addr:       "172.19.189.15:6379",
			PoolSize:   5,
			MaxRetries: 2,
			Password:   "testredis",
			DB:         1,
		},
		"http://172.19.198.26:15201/awsi_sec9/services/LoginService",
	)

	for i := 0; i < 5; i++ {
		token, err := ccbsTokenClient.Request("tru3", "Unix11")
		if err != nil {
			log.Fatal(err.Error())
		}
		fmt.Println("-> Token", token)
	}

}
