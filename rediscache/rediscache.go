package rediscache

import (
	"strings"
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/ft25/omx/go-ccbstoken/awsisec9"
)

const (
	tokenNameSpace    = "IOMX:TOKEN:{{USER}}"
	tokenTTLNameSpace = "IOMX:TOKEN:TTL:{{USER}}"
)

//CCBSTokenClient struct
type CCBSTokenClient struct {
	redisClient      *redis.Client
	ccbsLoginService *awsisec9.Sec9Service
}

//Create connection
func (e *CCBSTokenClient) Create(redisOpt redis.Options, tokenService string) {
	e.redisClient = redis.NewClient(&redisOpt)
	e.ccbsLoginService = new(awsisec9.Sec9Service)
	e.ccbsLoginService.Create(tokenService)
}

//Request token
func (e *CCBSTokenClient) Request(username string, password string) (string, error) {

	key := strings.Replace(tokenNameSpace, "{{USER}}", username, -1)
	keyTTL := strings.Replace(tokenTTLNameSpace, "{{USER}}", username, -1)

	tokenTTL, err := e.redisClient.Get(keyTTL).Result()
	if err != nil && tokenTTL == "" {

		//Logout expire token
		tokenExpire, err := e.redisClient.Get(key).Result()
		e.ccbsLoginService.Logout(tokenExpire)

		newToken, err := e.ccbsLoginService.Login(username, password)
		if newToken != "" {
			e.redisClient.Set(key, newToken, 0)
			e.redisClient.SetNX(keyTTL, newToken, 1000*time.Second)
			return newToken, nil
		}
		return "", err

	}
	return tokenTTL, nil

}
