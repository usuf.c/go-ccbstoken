package awsisec9

import (
	"bytes"
	"crypto/tls"
	"encoding/xml"
	"errors"
	"log"
	"net/http"
	"strings"
)

const (
	loginRequestConst = `<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
							<Body>
								<login xmlns="http://ws.ident.uams.amdocs">
									<userId>{{username}}</userId>
									<password>{{password}}</password>
								</login>
							</Body>
						</Envelope>`
	logoutRequestConst = `<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
							<Body>
								<logout xmlns="http://ws.ident.uams.amdocs">
									<ticket>{{token}}</ticket>
								</logout>
							</Body>
						</Envelope>`
	isTicketValidRequestConst = `<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
							<Body>
								<isTicketValid xmlns="http://ws.ident.uams.amdocs">
									<ticket>to{{token}}ken</ticket>
								</isTicketValid>
							</Body>
						</Envelope>`
)

//Sec9Service struct
type Sec9Service struct {
	url string
}

//SoapFaultResponse struct
type SoapFaultResponse struct {
	XMLName xml.Name
	Body    struct {
		XMLName xml.Name
		Fault   struct {
			XMLName     xml.Name
			Faultcode   string `xml:"faultcode"`
			Faultstring string `xml:"faultstring"`
			Detail      struct {
				XMLName xml.Name
				Fault   struct {
					XMLName          xml.Name
					ErrorCode        string `xml:"errorCode"`
					ErrorDescription string `xml:"errorDescription"`
				} `xml:"fault"`
			} `xml:"detail"`
		}
	}
}

//LoginResponse struct
type LoginResponse struct {
	XMLName xml.Name
	Body    struct {
		XMLName       xml.Name
		LoginResponse struct {
			XMLName     xml.Name
			LoginReturn string `xml:"loginReturn"`
		} `xml:"loginResponse"`
	}
}

//IsTicketValidResponse struct
type IsTicketValidResponse struct {
	XMLName xml.Name
	Body    struct {
		XMLName               xml.Name
		IsTicketValidResponse struct {
			XMLName             xml.Name
			IsTicketValidReturn bool `xml:"isTicketValidReturn"`
		} `xml:"isTicketValidResponse"`
	}
}

//Create AwsiSec9Service
func (e *Sec9Service) Create(url string) {
	e.url = url
}

//Login service
func (e *Sec9Service) Login(username string, password string) (string, error) {

	// wsdl service url
	url := e.url

	loginRequest := strings.TrimSpace(loginRequestConst)

	loginRequest = strings.Replace(loginRequest, "{{username}}", username, -1)
	loginRequest = strings.Replace(loginRequest, "{{password}}", password, -1)

	// payload
	payload := []byte(loginRequest)

	httpMethod := "POST"

	// soap action
	soapAction := "urn:login"

	req, err := http.NewRequest(httpMethod, url, bytes.NewReader(payload))
	if err != nil {
		log.Fatal("Error on creating request object. ", err.Error())
		return "", err
	}

	// set the content type header, as well as the oter required headers
	req.Header.Set("Content-type", "text/xml")
	req.Header.Set("SOAPAction", soapAction)

	// prepare the client request
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	// dispatch the request
	res, err := client.Do(req)
	if err != nil {
		log.Fatal("Error on dispatching request. ", err.Error())
		return "", err
	}

	// read and parse the response body
	result := new(LoginResponse)
	err = xml.NewDecoder(res.Body).Decode(result)
	if err != nil {
		log.Fatal("Error on unmarshaling xml. ", err.Error())
		return "", err
	}

	tokenReturn := result.Body.LoginResponse.LoginReturn

	if tokenReturn == "" {
		err := errors.New("CCBS Login exception")
		return "", err
	}

	return tokenReturn, nil
}

//Logout service
func (e *Sec9Service) Logout(token string) (bool, error) {
	// wsdl service url
	url := e.url

	logoutRequest := strings.TrimSpace(logoutRequestConst)

	logoutRequest = strings.Replace(logoutRequest, "{{token}}", token, -1)

	// payload
	payload := []byte(logoutRequest)

	httpMethod := "POST"

	// soap action
	soapAction := "urn:logout"

	req, err := http.NewRequest(httpMethod, url, bytes.NewReader(payload))
	if err != nil {
		return false, err
	}

	// set the content type header, as well as the oter required headers
	req.Header.Set("Content-type", "text/xml")
	req.Header.Set("SOAPAction", soapAction)

	// prepare the client request
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	// dispatch the request
	res, err := client.Do(req)
	if err != nil {
		log.Fatal("Error on dispatching request. ", err.Error())
		return false, err
	}

	log.Println("-> Logout completed", res)

	return true, nil
}

//IsTicketValid service
func (e *Sec9Service) IsTicketValid(token string) (bool, error) {
	// wsdl service url
	url := e.url

	isTicketValidRequest := strings.TrimSpace(isTicketValidRequestConst)

	isTicketValidRequest = strings.Replace(isTicketValidRequest, "{{token}}", token, -1)

	// payload
	payload := []byte(isTicketValidRequest)

	httpMethod := "POST"

	// soap action
	soapAction := "urn:isTicketValid"

	req, err := http.NewRequest(httpMethod, url, bytes.NewReader(payload))
	if err != nil {
		log.Fatal("Error on creating request object. ", err.Error())
		return false, err
	}

	// set the content type header, as well as the oter required headers
	req.Header.Set("Content-type", "text/xml")
	req.Header.Set("SOAPAction", soapAction)

	// prepare the client request
	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	// dispatch the request
	res, err := client.Do(req)
	if err != nil {
		log.Fatal("Error on dispatching request. ", err.Error())
		return false, err
	}

	// read and parse the response body
	result := new(IsTicketValidResponse)
	err = xml.NewDecoder(res.Body).Decode(result)
	if err != nil {
		log.Fatal("Error on unmarshaling xml. ", err.Error())
		return false, err
	}

	log.Println("-> IsTicketValid completed")

	// print the users data
	isTicketValidReturn := result.Body.IsTicketValidResponse.IsTicketValidReturn

	return isTicketValidReturn, nil
}
